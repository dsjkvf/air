air
===


## About

`air` is a simple shell script to print out the air quality indices obtained from https://airly.org/ and/or https://airvisual.com/. Additionally included `extras/weather` is a sample script to produce some reporting.

## Dependencies

`air` depends on `jq` as well on some usual shell utilities, like `curl` or `awk`.
